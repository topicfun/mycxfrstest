package hu.fornax.olir3.dms.producer;

import java.util.HashMap;
import java.util.Map;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.camel.CamelContext;
import org.apache.camel.cdi.ContextName;
import org.apache.camel.component.cxf.CxfComponent;
import org.apache.camel.component.cxf.CxfEndpoint;
import org.apache.camel.component.cxf.DataFormat;
import org.apache.cxf.ws.security.wss4j.WSS4JOutInterceptor;

@Named("dms_producer")
public class DMSWSProducer {

    private static final String CXF_ENDPOINT_URI = "http://localhost:8080/webservices/greeting";

    /** A/Az camel context. */
    @Inject
    @ContextName("camel-dms-context")
    CamelContext camelContext;

    @Named("dmsProducerEndpoint")
    @Produces
    public CxfEndpoint createCxfProducerEndpoint() {
        CxfComponent cxfProducerComponent = new CxfComponent(this.camelContext);

        CxfEndpoint cxfProducerEndpoint = new CxfEndpoint(CXF_ENDPOINT_URI, cxfProducerComponent);

        cxfProducerEndpoint.setBeanId("dmsProducerEndpoint");
        cxfProducerEndpoint.setServiceClass(GreetingService.class);
        cxfProducerEndpoint.setMtomEnabled(true);
        cxfProducerEndpoint.setAllowStreaming(true);
        cxfProducerEndpoint.setDataFormat(DataFormat.POJO);
        cxfProducerEndpoint.setLoggingFeatureEnabled(true);
        cxfProducerEndpoint.setSynchronous(true);

        Map<String, Object> outProps = new HashMap<String, Object>();
        outProps.put("action", "UsernameToken");
        outProps.put("passwordType", "PasswordText");
        outProps.put("mustUnderstand", "0");

        outProps.put("user", "zz-OLIR3-svc");
        outProps.put("passwordCallbackClass", "hu.fornax.olir3.dms.producer.UTPasswordCallback");

        WSS4JOutInterceptor wssOut = new WSS4JOutInterceptor(outProps);
        cxfProducerEndpoint.getOutInterceptors().add(wssOut);

        return cxfProducerEndpoint;
    }

}
