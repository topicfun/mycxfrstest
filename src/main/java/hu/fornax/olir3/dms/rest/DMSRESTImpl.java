package hu.fornax.olir3.dms.rest;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.camel.ProducerTemplate;
import org.apache.camel.cdi.ContextName;

import hu.fornax.olir3.common.log.Logger;
import hu.fornax.olir3.file.protocol.rest.DMSREST;

@Named("dmsRESTImpl")
public class DMSRESTImpl implements DMSREST {

    @Inject
    private Logger log;

    @Inject
    @ContextName("camel-dms-context")
    ProducerTemplate producerTemplate;

    /*
     * (non-Javadoc)
     * 
     * @see hu.fornax.olir3.file.protocol.rest.DMSREST#downloadDMSFile(java.lang.String)
     */
    @Override
    public Response downloadDMSFile(String dmsId) {

        log.debug("dms id: {}", dmsId);

        Object[] serviceParams = new Object[] { "message", "name" };

        String stringResponse = producerTemplate.requestBody("direct:dmsCall", serviceParams, String.class);
        log.debug("stringresponse: {}", stringResponse);

        ResponseBuilder response = Response.ok();

        return response.build();
    }

}
