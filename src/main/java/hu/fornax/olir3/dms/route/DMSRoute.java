package hu.fornax.olir3.dms.route;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.cdi.ContextName;
import org.apache.camel.component.cxf.CxfEndpoint;

import hu.fornax.olir3.util.esb.temp.ESBLog;

/**
 * <p>
 * <b>DMSRoute osztály.</b>
 * </p>
 *
 * @author Zoltan Balogh <br>
 * 
 *         <p>
 *         <b>Attribútumok:</b>
 *         </p>
 *         <ul>
 *         <li></li>
 *         </ul>
 */
@ApplicationScoped
@ContextName("camel-dms-context")
public class DMSRoute extends RouteBuilder {

    @Inject
    @Named("dmsProducerEndpoint")
    CxfEndpoint dmsProducerEndpoint;

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.camel.builder.RouteBuilder#configure()
     */
    @Override
    public void configure() throws Exception {

        from("direct:dmsCall")
                .log(ESBLog.DMS + " sending DMS request body  ${headers}")
                .to(this.dmsProducerEndpoint)
                .log(ESBLog.DMS + " received DMS response body  ${headers}");

        from("cxfrs:http://localhost:8080/olir3-dms2/rest?resourceClasses=hu.fornax.olir3.dms.producer.GreetingServiceRS&loggingFeatureEnabled=true")
                .log(ESBLog.DMS + "incoming REST request: ${body} ${headers}")
                .bean("dmsRESTImpl");

    }

}
